---
title: CEDARO report:SCHISTO-MAL-STH
subtitle: Schistosoma haematobium effects on Plasmodium falciparum infection modified by soiltransmitted helminths 
      in school-age children living in rural areas of Gabon.
output:
  pdf_document: 
  toc : yes
header-includes:
 - \usepackage{fancyhdr}
---
\addtolength{\headheight}{1.0cm} 
\pagestyle{fancyplain} 
\lhead{\includegraphics[height=1.5cm]{cermel-logo}}
\renewcommand{\headrulewidth}{0pt} 

# Background

The “CERMEL Data Repository” (CEDARO) aims to archive data of all research projects performed at
CERMEL. A detailed description of the repository is available from the CEDARO team (cedaro@cermel.org).
Before archiving, a codebook and an anonymized dataset are generated. In addition, a basic statistical analysis
is performed to ensure that the data in the publication corresponds to the submitted dataset. Typically, only
data to ensure that the numbers of the basic characteristics of the study population and of the outcomes are correct.
Rarely would effect sizes, regression models etc. be verified.
A report comparing the original published results with that of the CEDARO analysis is generated and sent
to the investigator who submitted the data set. The CEDARO team will follow-up on any descripancies to
make sure that the analysis and data are valid.

# Publication

**Reference**

Dejon-Agobe JC, Zinsou JF, Honkpehedji YJ, Ateba-Ngoa U, Edoa J-R, Adegbite BR, et al.(2018) Schistosoma haematobium effects on
Plasmodium falciparum infection modified by soiltransmitted helminths in school-age children living in rural areas of Gabon.
PLoS Negl Trop Dis 12(8): e0006663

DOI: 10.1371/journal.pntd.0006663

PubMed PMID : 30479248

PubMed-Central PMCID:  PMC6335900

**Abstract**

"Malaria burden remains high in the sub-Saharan region where helminths are prevalent and where children are often infected with both types of parasites. 
Although the effect of helminths on malaria infection is evident, the impact of these co-infections is not clearly elucidated yet and the scarce findings are conflicting. 
In this study, we investigated the effect of schistosomiasis, considering soil-transmitted helminths (STH), on prevalence and incidence of Plasmodium falciparum infection."

```{r echo = FALSE, warning = FALSE, message = FALSE}

library(readxl)
library(dplyr)
library(stringr)
library(knitr)

```

# Analysis

"Among the participants who were invited to participate in the study, informed consent was granted for 754 children by their parents or 
their legal representatives. Of those, a total of 739 children with schistosomiasis and P. falciparum status available were included
at baseline (Fig 1). Among participants enrolled, 68 (9%) children were not able to provide sample stool at baseline and from the others, 
31% [95%CI: 27%-35%] were infected with STH. 
The most prevalent infection was trichuris with 21% [95%CI: 18%-24%] followed by ascaris and hookworm with 19% [95%CI: 16%-22%] and 
6% [95%CI: 5%-8%], respectively. Mean age of these study population was 10.4 (SD = 3.1) years, 
  the boy-to-girl sex ratio was 1.1:1 (Table 1). Of participants included, 586 (79%) agreed to be followed-up for malaria incidence."

```{r echo = FALSE, warning = FALSE, message = FALSE}


dt <- read_excel("data/S1_Dataset.xlsx", 
          sheet = "Baseline") 

npart <- dt %>%
         count() %>%
         mutate(variable = "Participants",
         res = as.character(n)) %>%
         select(variable, res)

nstool <- dt %>%
         filter(stools_sample == "no") %>%
         count(stools_sample) %>%
         mutate(perc = round(n * 100 / as.numeric(npart$res)),
          variable = case_when(stools_sample == "no" ~ "Not sample stool"),
          res = paste0(n, " (", perc, "%)"))  %>%
         select(variable, res)

ystool <- dt %>%
       count(`tri_ank_co-infection`, trichirus_qual_result,
                 ascaris_qual_result, ankylostome_qual_result) %>%
       slice(2:9) %>%
       mutate(variable = "Stools sample",
         res = as.character(sum(n))) %>%
         select(variable, res) %>%
       slice(1)

stool <- dt %>%
         count(`tri_ank_co-infection`, trichirus_qual_result,
                 ascaris_qual_result, ankylostome_qual_result) %>%
         slice(3:9) %>% 
         mutate(variable = "Participants with STH",
           resu = as.character(sum(n))) %>%
         select(variable, resu) %>%
         slice(1) %>%
         mutate(perc = round(as.numeric(resu) * 100 /       
                               as.numeric(ystool$res)),
         res = paste0(perc, "%"))  %>%
         select(variable, res) 


tri <- dt %>%
         count(trichirus_qual_result) %>%
         slice(2:3) %>%
         mutate(perc = round(n * 100 / sum(n)),
           res = paste0(perc, "%"), 
           variable = case_when(
             trichirus_qual_result== "positive" ~ "Trichirus pos"))  %>%
         select(variable, res) %>%
         slice(2) 

asca <- dt %>%
         count(ascaris_qual_result) %>%
         slice(2:3)  %>%
         mutate(perc = round(n * 100 / sum(n)),
            res = paste0(perc, "%"), 
            variable = case_when(
             ascaris_qual_result == "positive" ~ "Ascaris pos"))  %>%
         select(variable, res) %>%
         slice(2) 

anki <- dt %>%
         count(ankylostome_qual_result) %>%
         slice(2:3)  %>%
         mutate(perc = round(n * 100 / sum(n)),
          res = paste0(perc, "%"),
          variable = case_when(
          ankylostome_qual_result == "positive" ~ "Ankylostosome pos")) %>%
         select(variable, res) %>%
         slice(2) 

mean_age <- dt %>%
          summarise(mean = round(mean(age), 1),
            var = round(sd(age),1),
            res = paste0(mean, " (", var, ")")) %>%
          mutate(variable = "Mean age (SD)") %>%
          select(variable, res)


ratio <- round(sum(dt$sex == "male") / sum(dt$sex == "female"),1)

rat <- data_frame(variable = "Ratio (Male / Female)", 
                  res = as.character(ratio))

fol <- dt %>%
    count(`follow-up_phase`) %>%
    mutate(perc = round(n*100/sum(n), 1), 
           res = paste0(n, " (", perc , "%)"), 
           variable = "Followed-up malaria") %>%
    select(variable, res) %>%
    slice(2)

pfal <- dt %>%
    count(tbs_result) %>%
    mutate(perc = round(n * 100 / sum(n)),
          variable = case_when(
                      tbs_result == "positive" ~ "P . falciparum "),
          res = paste0(perc,"%"))  %>%
    select(variable, res) %>%
    slice(2)

sh <- dt %>%
    count(urine_qual_result) %>%
    mutate(perc = round(n * 100 / sum(n)),
          variable = case_when(
                      urine_qual_result == "positive" ~ "S.haematobium"),
          res = paste0(perc,"%"))  %>%
    select(variable, res) %>%
    slice(2)

coinf <- dt %>%
        rename(co_inf = "co-infection") %>%
        count(co_inf) %>%
        mutate(perc = round(n * 100 / sum(n)),
          variable = case_when(
                      co_inf == "yes" ~ "Co-infection"),
          res = paste0(n," (", perc,"%)"))  %>%
    select(variable, res) %>%
    slice(2)

fever <- dt %>%
       filter(tbs_result == "positive") %>%
       count(fever) %>%
       mutate(perc = round(n * 100 / sum(n)),
          variable = case_when(
                      fever == "yes" ~ "Particpant with P.fal and fever"),
          res = paste0(perc,"%"))  %>%
    select(variable, res) %>%
    slice(3)

res1 <- bind_rows(npart, nstool, ystool, stool, tri, asca)
res2 <- bind_rows(anki, mean_age, rat, fol, pfal, sh, coinf, fever)

Published <- c("739", "68 (9%)", "671", "31%", "21%", "19%", "6%", 
               "10.4 (3.1)", "1.1", "586 (79%)", "23%", "30%",
               "67 (9%)", "8%")

res_tot <- bind_rows(res1, res2) %>%
      rename(Variables = variable , Submit = res) %>%
      mutate(Published = c("739", "68 (9%)","671","31%", "21%", "19%",
                    "6%", "10.4 (3.1)", "1.1", "586 (79%)", "23%", "30%",
                "67 (9%)", "8%"),
         Discrepency = if_else(Submit == Published ,"no","yes"))

kable(res_tot, caption = "Part  results")


# vilpf <- dt %>%
#        filter(tbs_result == "positive") %>%
#        count(location)
# vilsh <- dt %>%
#        filter(urine_qual_result == "positive") %>%
#        count(location)


```


- CEDARO ID: SCHISTO-MAL-STH
- Date of report: 2020-05-12
- Analysis performed by: Eddy Mbena
- Analysis supervised by: Bertrand Lell & Fabrice Mougeni
- Raw data supplied by: Dejon-Agobe JC
- Code repository: https://gitlab.com/MBENA/schistosoma.git
- Local storage path: NAS

